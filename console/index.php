<?php

// AWS API
require 'AWSSDKforPHP/aws.phar';

// Include secret AWS keys
include 'aws_keys.php';

use Aws\Route53\Route53Client;
use Aws\Ec2\Ec2Client;

$REGION = 'us-west-1';
$AMI = 'ami-aca896e9';
$KEY_NAME = 'Sandbox';
$INSTANCE_TYPE = 'm1.small';
$SECURITY_GROUP_ID = 'sg-8c2830e0';
$SUBNET_ID = 'subnet-191df671';

$ec2 = Ec2Client::factory(array(
    'key'    => $AWS_ACCESS_KEY_ID,
    'secret' => $AWS_SECRET_ACCESS_KEY,
    'region' => $REGION
));

$route53 = Route53Client::factory(array(
    'key'    => $AWS_ACCESS_KEY_ID,
    'secret' => $AWS_SECRET_ACCESS_KEY 
));


if (isset($_GET['hostname'])) {
	$zones = $route53->listHostedZones();
	$zones = $zones->get('HostedZones');
	foreach($zones as $zone) {
		if($zone['Name'] == 'dev.goworks.org') {
			break;
		}
	}

	$records = $route53->ListResourceRecordSets(array('HostedZoneId'=>$zone['Id']));
	$records = $records->get('ResourceRecordSets');
	foreach($records as $record) {
		if($record['ResourceRecords'][0]['Value'] == $_SERVER['REMOTE_ADDR']) {
			$name = rtrim($record['Name'], '.');
			break;
		}
	}

	echo $name;
	exit(0);
}

if (isset($_GET['stop'])) {
	$ec2->stopInstances(array('InstanceIds'=>array($_GET['stop'])));
	header('Location: '.substr($_SERVER['REQUEST_URI'], 0, 0-strlen($_SERVER['QUERY_STRING'])));
	exit(0);
}

if (isset($_GET['start'])) {
	$ec2->startInstances(array('InstanceIds'=>array($_GET['start'])));
	header('Location: '.substr($_SERVER['REQUEST_URI'], 0, 0-strlen($_SERVER['QUERY_STRING'])));
	exit(0);
}

if (isset($_GET['run'])) {
	$reservation = $ec2->runInstances(array('ImageId'=>$AMI,
						'MinCount'=>1,
						'MaxCount'=>1,
						'KeyName'=>$KEY_NAME,
						'InstanceType'=>$INSTANCE_TYPE,
						'InstanceInitiatedShutdownBehavior'=>'stop',
						'SubnetId'=>$SUBNET_ID,
						'SecurityGroupIds'=>array($SECURITY_GROUP_ID)
						));
	$ec2->createTags(array('Resources'=>array($reservation['Instances'][0]['InstanceId']), 'Tags'=>array(array('Key'=>'Name', 'Value'=>$_GET['run']))));

	$ip = $reservation['Instances'][0]['PrivateIpAddress'];
	$zones = $route53->listHostedZones();
	$zones = $zones->get('HostedZones');
	$update_zones = array();
	foreach($zones as $zone) {
		if($zone['Name'] == 'dev.goworks.org.' || $zone['Name'] == 'dev.groundworkopportunities.org.') {
			array_push($update_zones, $zone);
		}
	}
	foreach($update_zones as $zone) {
		$records = $route53->ListResourceRecordSets(array('HostedZoneId'=>$zone['Id']));
		$records = $records->get('ResourceRecordSets');
		$domain = $_GET['run'].'.'.$zone['Name'];
		foreach($records as $record) {
			if($record['Name'] == $domain) {
				$route53->changeResourceRecordSets(array('HostedZoneId'=>$zone['Id'],
									 'ChangeBatch'=>array('Changes'=>array(array('Action'=>'DELETE',
													       'ResourceRecordSet'=>array('Name'=>$domain,
												 					  'Type'=>'A',
																	  'TTL'=>$record['TTL'],
															  		  'ResourceRecords'=>array(array('Value'=>$record['ResourceRecords'][0]['Value']))
			
										 							 )
													      )))
									));
			}
		}
		$route53->changeResourceRecordSets(array('HostedZoneId'=>$zone['Id'],
							 'ChangeBatch'=>array('Changes'=>array(array('Action'=>'CREATE',
											       'ResourceRecordSet'=>array('Name'=>$domain,
											 				  'Type'=>'A',
															  'TTL'=>60,
															  'ResourceRecords'=>array(array('Value'=>$ip))
															)
											     )))
							));
	}

	header('Location: '.substr($_SERVER['REQUEST_URI'], 0, 0-strlen($_SERVER['QUERY_STRING'])));
	exit(0);
}

if (isset($_GET['refresh-uploads'])) {
	$cmd = 'ssh -o "StrictHostKeyChecking no" -i /Sandbox.priv ubuntu@'.$_GET['refresh-uploads'].'.dev.goworks.org /home/ubuntu/code4go/sandbox/refresh-uploads.sh';
	exec($cmd);
	header('Location: '.substr($_SERVER['REQUEST_URI'], 0, 0-strlen($_SERVER['QUERY_STRING'])));
	exit(0);
}

if (isset($_GET['refresh-db'])) {
	$cmd = 'ssh -o "StrictHostKeyChecking no" -i /Sandbox.priv ubuntu@'.$_GET['refresh-db'].'.dev.goworks.org /home/ubuntu/code4go/sandbox/refresh-db.sh';
	exec($cmd);
	header('Location: '.substr($_SERVER['REQUEST_URI'], 0, 0-strlen($_SERVER['QUERY_STRING'])));
	exit(0);
}

echo "<center>";
echo "<img src='http://groundworkopportunities.org/wp-content/uploads/go_black.png'/><br/>";
echo "<h2>Groundwork Opportunities Development Console</h2>";

// Print out the new instance form
echo <<<EOL
 <form>
  New Instance Name: <input type="text" name="run"></input>
  <input type="submit" name="" title="Create" method="GET">
 </form>
EOL;

// List instances


echo '<table>';
echo ' <tr>';
echo '  <th>Server</th>';
echo '  <th>Status</th>';
echo '  <th width="50px"></th>';
echo '  <th></th>';
echo '  <th></th>';
echo '  <th></th>';
echo ' <tr/>';

echo '<pre>';

$reservations = $ec2->describeInstances(array());
$reservations = $reservations->get('Reservations');
foreach($reservations as $reservation) {
	foreach($reservation['Instances'] as $instance) {
		foreach($instance['SecurityGroups'] as $sg) {
			if($sg['GroupName'] == 'SandboxVMs') {
				foreach($instance['Tags'] as $tag) {
					if($tag['Key'] == 'Name') {
						echo '<tr>';
						echo '<td>'.$tag['Value'].'</td>';
						echo '<td>'.$instance['State']['Name'].'</td>';
						echo '<td>[';
						if($instance['State']['Name'] == 'stopped') {
							echo '<a href="?start='.$instance['InstanceId'].'">Start</a>';
						} else if($instance['State']['Name'] == 'running') {
							echo '<a href="?stop='.$instance['InstanceId'].'">Stop</a>';
						}
						echo ']</td>';
						echo "<td><b>Refresh:</b></td>";
						echo "<td><a href='?refresh-db=".$tag['Value']."'>[Database]</a></td>";
						echo "<td><a href='?refresh-uploads=".$tag['Value']."'>[Uploads]</a></td>";
						echo '</tr>';
					}
				}
			}
		}
	}
}

echo "</center>";

//print_r($reservations);
