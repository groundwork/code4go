#!/bin/bash

echo ""
echo "This script will clean the system in order to make a fresh AMI image."
echo "*** ALL CODE AND DATABASES WILL BE DESTROYED ***"
echo -n "Are you sure you want to continue? [Y/n]: "
read n
echo ""

if [ "${n}" == "Y" ]; then

	echo "removing /home/ubuntu/go"
	sudo rm -rf /home/ubuntu/go

	echo "removing logs"
	sudo rm -rf /home/ubuntu/logs/*

	echo "dropping the 'go_wordpress' database"
	mysql -u root -pCode4Go -e 'drop database go_wordpress'

	echo "updating the code4go and go.clean checkouts"
	cd /home/ubuntu/go.clean
	git pull
	cd /home/ubuntu/code4go
	git pull

	echo "All done, go make your AMI!  Make sure your home directory is clean."

else
	echo "Aborted"
fi


