#!/bin/bash

USER='root'
PASS='Code4Go'
DIR='/home/ubuntu/code4go'
DOMAIN=`hostname`

# Pull down the latest sanitized sql dump
s3cmd get s3://go-production-mysql/mysql.clean.latest.sql.bz2 ${DIR}/mysql.clean.latest.sql.bz2
bunzip2 ${DIR}/mysql.clean.latest.sql.bz2

# drop prior database
mysql -u ${USER} -p${PASS} -e "drop database go_wordpress;"
mysql -u ${USER} -p${PASS} -e "create database go_wordpress;"

# Load latest DB
mysql -u ${USER} -p${PASS} go_wordpress < ${DIR}/mysql.clean.latest.sql
rm ${DIR}/mysql.clean.latest.sql

# now we need to update any URLs to the new domain
cd "${DIR}/sandbox/Search-Replace-DB"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "http://www.groundworkopportunities.org.php53-22.ord1-1.websitetestlink.com" --replace "http://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "https://www.groundworkopportunities.org.php53-22.ord1-1.websitetestlink.com" --replace "https://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "http://www.groundworkopportunities.org" --replace "http://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "https://www.groundworkopportunities.org" --replace "https://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "http://groundworkopportunities.org" --replace "http://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "https://groundworkopportunities.org" --replace "https://${DOMAIN}"


