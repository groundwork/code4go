#!/bin/bash

# Check if we've been running for more than 30 minutes
if [ `cat /proc/uptime | awk -F. '{print $1}'` -gt 1800 ]; then 
	# If no users are actively logged into the system than shutdown
	users | grep ubuntu || /sbin/halt
fi;

