#!/bin/bash

DIR='/home/ubuntu/go/wp-content/uploads'

# ensure path is setup
if [ ! -d ${DIR} ]; then
	mkdir ${DIR}
fi;

# Sync down uploads from production copy
s3cmd sync --no-preserve s3://go-production-uploads/ ${DIR}/

# change owner to apache so it can write files
sudo chown www-data ${DIR}

