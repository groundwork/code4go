#! /bin/bash
########################################################
#
#  Script: install.sh
#  Description:  Setup initial usage of GO site
#
########################################################

##### Default Constants
GOGITPREFIX=https
GOGITPATH=bitbucket.org/groundwork
SITEPATH=.
DBNAME=go_wordpress
DBHOST=localhost
DBTABLEPREFIX=wp_
TEMPLATECONFIGFILE="$SITEPATH/wp-config.php.template"
TEMPCONFIGFILE="$SITEPATH/wp-config.php.temp"
CONFIGFILE="$SITEPATH/wp-config.php"

##### Functions
function usage
{
    echo "usage: install.sh [[[-u git_username ]] [-p git_password ] [-o install_path ] | [-h]]"
}

##### Main
# Loop until all parameters are used up
while [ "$1" != "" ]; do
	case $1 in
        -d | --site_domain )    shift
                                GODOMAIN=$1
                                ;;
        -u | --git_username )   shift
                                GITUSER=$1
                                ;;
        -p | --git_password )   shift
                                GITPASSWORD=$1
                                ;;
        -n | --db_name )        shift
                                DBNAME=$1
                                ;;
        -h | --db_host )        shift
                                DBHOST=$1
                                ;;        
        -m | --db_user )        shift
                                DBUSER=$1
                                ;;          
        -n | --db_password )    shift
                                DBPASSWORD=$1
                                ;;
        -t | --db_table_prefix) shift
                                DBTABLEPREFIX=$1
                                ;;                       
        -o | --outpath )        shift
                                SITEPATH=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    # Shift all the parameters down by one
    shift

done

while [ "$GODOMAIN" = "" ]; do
	echo "Site domain (e.g. www.goworks.org):" 
	read GODOMAIN
done

while [ "$GITUSER" = "" ]; do
	echo "Username for GO Git Repository:" 
	read GITUSER
done

while [ "$GITPASSWORD" = "" ]; do
	echo "Password for GO Git Repository:" 
	read GITPASSWORD
done

while [ "$DBUSER" = "" ]; do
	echo "Username for mySQL database:" 
	read DBUSER
done

while [ "$DBPASSWORD" = "" ]; do
	echo "Password for mySQL database:" 
	read DBPASSWORD
done


git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/go.git $SITEPATH
git clone git://github.com/WordPress/WordPress.git $SITEPATH/wordpress
git clone https://github.com/Rahe/Simple-image-sizes $SITEPATH/wp-content/plugins/simple-image-sizes
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/buddypress.git $SITEPATH/wp-content/plugins/buddypress
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/bp-album.git $SITEPATH/wp-content/plugins/bp-album
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/bp-registration-options.git $SITEPATH/wp-content/plugins/bp-registration-options
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/cron-view.git $SITEPATH/wp-content/plugins/cron-view
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/w3-total-cache.git $SITEPATH/wp-content/plugins/w3-total-cache
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/wordpress-importer.git $SITEPATH/wp-content/plugins/wordpress-importer
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/wordpress-seo.git $SITEPATH/wp-content/plugins/wordpress-seo
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/wufoo-shortcode.git $SITEPATH/wp-content/plugins/wufoo-shortcode
git clone $GOGITPREFIX://$GITUSER:$GITPASSWORD@$GOGITPATH/wp-recaptcha.git $SITEPATH/wp-content/plugins/wp-recaptcha


#Update DB Name
OLD="define('DB_NAME', 'go_wordpress');"
NEW="define('DB_NAME', '$DBNAME');"
sed -u "s/$OLD/$NEW/g" "$TEMPLATECONFIGFILE" > "$TEMPCONFIGFILE"
mv "$TEMPCONFIGFILE" "$CONFIGFILE"

#Update DB User
OLD="define('DB_USER', 'root');"
NEW="define('DB_USER', '$DBUSER');"
sed -u "s/$OLD/$NEW/g" "$CONFIGFILE" > "$TEMPCONFIGFILE"
mv "$TEMPCONFIGFILE" "$CONFIGFILE"

#Update DB Password
OLD="define('DB_PASSWORD', 'GO_PASS');"
NEW="define('DB_PASSWORD', '$DBPASSWORD');"
sed -u "s/$OLD/$NEW/g" "$CONFIGFILE" > "$TEMPCONFIGFILE"
mv "$TEMPCONFIGFILE" "$CONFIGFILE"

#Update DB Host
OLD="define('DB_HOST', 'localhost');"
NEW="define('DB_HOST', '$DBHOST');"
sed -u "s/$OLD/$NEW/g" "$CONFIGFILE" > "$TEMPCONFIGFILE"
mv "$TEMPCONFIGFILE" "$CONFIGFILE"

#Update DB Table Prefix
OLD="$table_prefix  = 'wp_';"
NEW="$table_prefix  = '$DBTABLEPREFIX';"
sed -u "s/$OLD/$NEW/g" "$CONFIGFILE" > "$TEMPCONFIGFILE"
mv "$TEMPCONFIGFILE" "$CONFIGFILE"

#Update Site Domain
OLD="GO_DOMAIN"
NEW="$GODOMAIN"
sed -u "s/$OLD/$NEW/g" "$CONFIGFILE" > "$TEMPCONFIGFILE"
mv "$TEMPCONFIGFILE" "$CONFIGFILE"