#!/bin/bash

PASS='Code4Go'
DIR='/home/ubuntu/code4go'

# Pull down latest production database
s3cmd get s3://go-production-mysql/mysql.latest.sql.bz2 ${DIR}/mysql.latest.sql.bz2
bunzip2 ${DIR}/mysql.latest.sql.bz2

# load into mysql
mysql -u root -pCode4Go -e "create database go_wordpress;"
mysql -u root -p${PASS} go_wordpress < $DIR/mysql.latest.sql
rm ${DIR}/mysql.latest.sql

# sanitize data
mysql -u root -p${PASS} go_wordpress < ${DIR}/remote/go-clean-db.sql

# dump new database
mysqldump -u root -p${PASS} go_wordpress > ${DIR}/mysql.clean.latest.sql
mysql -u root -p${PASS} -e "drop database go_wordpress;"
bzip2 ${DIR}/mysql.clean.latest.sql

# upload back to S3 for development instances
s3cmd put ${DIR}/mysql.clean.latest.sql.bz2 s3://go-production-mysql/
rm ${DIR}/mysql.clean.latest.sql.bz2

