#!/bin/bash

PASS='Code4Go'
DIR='/home/ubuntu/tools'
DOMAIN=`hostname`

echo "Importing MySQL database from go_wordpress.sql, previous version will be dumped to go_wordpress.sql.bak"

mysqldump -u root -p$PASS > $DIR/go_wordpress.sql.bak
echo " DROP DATABASE IF EXISTS go_wordpress; CREATE DATABASE go_wordpress;" | mysql -u root -p$PASS 
mysql -u root -p$PASS go_wordpress < $DIR/go_wordpress.sql

# now we need to update any URLs to the new domain
cd "${DIR}/Search-Replace-DB"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "http://www.groundworkopportunities.org.php53-22.ord1-1.websitetestlink.com" --replace "http://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "https://www.groundworkopportunities.org.php53-22.ord1-1.websitetestlink.com" --replace "https://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "http://www.groundworkopportunities.org" --replace "http://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "https://www.groundworkopportunities.org" --replace "https://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "http://groundworkopportunities.org" --replace "http://${DOMAIN}"
./searchreplacedb2cli.php --host localhost --user root -d go_wordpress --pass ${PASS} --search "https://groundworkopportunities.org" --replace "https://${DOMAIN}"


