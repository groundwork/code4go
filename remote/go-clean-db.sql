# go-clean-db.sql
# Cleans the DB of user passwords, emails, and order data and creates generic admin user


# Remove passwords 
#UPDATE `wp_users` SET user_pass='NO PASS', user_email='NO EMAIL';
UPDATE `wp_users` SET user_pass='NO PASS';


# Insert generic admin user
INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(111111, 'admin', '$P$BR.TpdfSNr8nlpu1VWVCUAjQJ6sAlE1', 'admin', 'shire@tekrat.com', '', '2013-02-01 01:34:20', '', 0, 'admin');

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`) VALUES
(111111, 'first_name', ''),
(111111, 'last_name', ''),
(111111, 'nickname', 'admin'),
(111111, 'description', ''),
(111111, 'rich_editing', 'true'),
(111111, 'comment_shortcuts', 'false'),
(111111, 'admin_color', 'fresh'),
(111111, 'use_ssl', '0'),
(111111, 'show_admin_bar_front', 'true'),
(111111, 'wp_capabilities', 'a:1:{s:13:"administrator";s:1:"1";}'),
(111111, 'wp_user_level', '10'),
(111111, 'dismissed_wp_pointers', 'wp330_toolbar,wp330_media_uploader,wp330_saving_widgets,wp340_choose_image_from_library,wp340_customize_current_theme_link');


# Clear out user data
UPDATE `wp_usermeta` SET 
meta_value='' WHERE
meta_key='first_name' OR
meta_key='last_name' OR
meta_key='billing_first_name' OR
meta_key='billing_last_name' OR
meta_key='billing_address_1' OR
meta_key='billing_address_2' OR
meta_key='billing_city' OR
meta_key='billing_postcode' OR
meta_key='billing_country' OR
meta_key='billing_state' OR
meta_key='billing_email' OR
meta_key='billing_phone' OR
meta_key='shipping_first_name' OR
meta_key='shipping_last_name' OR
meta_key='shipping_address_1' OR
meta_key='shipping_address_2' OR
meta_key='shipping_city' OR
meta_key='shipping_postcode' OR
meta_key='shipping_country' OR
meta_key='shipping_state' OR
meta_key='shipping_email' OR
meta_key='shipping_phone';


# Remove all orders
DELETE FROM wp_posts WHERE post_type='shop_order';
