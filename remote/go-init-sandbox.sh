#!/bin/bash

HOME="/home/ubuntu"
USER=`echo $HOSTNAME | sed -r 's/([^.]*)\.(.*)/\1/'`
DOMAIN=`echo $HOSTNAME`
PASS='Code4Go'

# Sed Replacement script:
SED_VARS="-e s/GO_USER/${USER}/g      \
		  -e s/GO_DOMAIN/${DOMAIN}/g  \
		  -e s/GO_PASS/${PASS}/g      \
		 ";

# Include our go bash settings in the user login scripts:
#echo "if [ -f ${HOME}/tools/go-bashrc.sh ]; then . ${HOME}/tools/go-bashrc.sh; fi" >> ${HOME}/.profile

# Update our custom bashrc with user info
sed -i.orig ${SED_VARS} ${HOME}/tools/go-bashrc.sh

# Replace the go_wordpress Database with the latest export
${HOME}/tools/go-mysql-import.sh ${DOMAIN}

# Update latest code, and checkout branch for user
#  We'll use our clean go.git.orig directory to seed the working directory
cd ${HOME}/go
git checkout master
git pull
git branch -r | grep "origin/${USER}" || git push origin HEAD:refs/heads/${USER}
git fetch
git checkout ${USER}
git submodule init
git submodule update

# configure git behavior
git config --global core.editor vim
git config --global user.name ${USER}
git config --global user.email ${EMAIL}
git config user.name ${USER}
git config user.email ${EMAIL}

# update our ctags file, path should be relevant so remote mounts work
cd ${HOME}/go
ctags -f .tags --recurse=yes . 2> /dev/null

# Create new login MOTD
sed -i.orig ${SED_VARS} ${HOME}/tools/go-motd
sudo cp ${HOME}/tools/go-motd /etc/motd.tail

# Update our wp-config.php file
cp ${HOME}/go/wp-config.php.template ${HOME}/go/wp-config.php
sed -i.orig ${SED_VARS} ${HOME}/go/wp-config.php

# change ownership of the upload directory
sudo chown -R www-data ${HOME}/go/wp-content/uploads 

# copy over the quickstart guide
cp ${HOME}/tools/quickstart.php ${HOME}/go/quickstart.php


