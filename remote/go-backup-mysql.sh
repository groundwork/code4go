#!/bin/bash

DATE=`date +%Y.%m.%d.%s`
S3PATH="s3://go-production-mysql"
HOME="/home/ubuntu"

MYSQL_USER="goworks"
MYSQL_PASS="g00dtog0"
MYSQL_HOST="productiondb.cnsag7sb8xij.us-west-1.rds.amazonaws.com"
MYSQL_DBS="go_production_wordpress"

# Backup DB
mysqldump -u ${MYSQL_USER} -p${MYSQL_PASS} -h ${MYSQL_HOST} ${MYSQL_DBS} | bzip2 > ${HOME}/mysql.${DATE}.sql.bz2 
s3cmd put ${HOME}/mysql.${DATE}.sql.bz2 ${S3PATH}/mysql.${DATE}.sql.bz2
s3cmd cp ${S3PATH}/mysql.${DATE}.sql.bz2 ${S3PATH}/mysql.latest.sql.bz2
rm ${HOME}/mysql.${DATE}.sql.bz2
