#!/bin/bash

# Profile/Environment settings for GO Users, ran at login

export PATH="${PATH}:/home/ubuntu/tools"

alias .g='git'
alias .b='git branch'
alias .c='git checkout'
