import sublime, sublime_plugin, os

class transferOnSave(sublime_plugin.EventListener):

	def on_post_save(self, view):
		print 'transferOnSave: on_post_save'
		remote_path = view.settings().get('transferOnSave_remotePath')
		local_path = view.settings().get('transferOnSave_localPath')
		host = view.settings().get('transferOnSave_host')
		local_file = view.file_name()

		should_transfer = view.settings().get('transferOnSave_enabled')
		if should_transfer == 1:
			if local_file.find(local_path) == 0:
				remote_file = remote_path+local_file[len(local_path):]
				view.window().run_command('exec', {'cmd':['scp', local_file, host+':'+remote_file]})
		else:
			print 'transferOnSave: Project not configured for transferOnSave.  Try setting transferOnSave_enabled in project settings.'