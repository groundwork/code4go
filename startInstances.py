#!/usr/bin/env python


import boto

from boto.ec2.connection import EC2Connection

import sys, getopt

import os
from time import sleep

import subprocess


ami='ami-fe1e33bb'  # Sandbox-2013.02.06
key_name='Sandbox'
instance_type='m1.small'
security_groups=['Hack Event']

region_id='us-west-1'
aws_access_key_id='AKIAJQBCKA7D7KAV7YAQ'
aws_secret_access_key='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'

tool_dir='/Users/shire/data/code4go/tools'
remote_tool_dir='/home/ubuntu/tools'

os.environ['AWS_ACCESS_KEY_ID'] = aws_access_key_id;
os.environ['AWS_SECRET_ACCESS_KEY'] = aws_secret_access_key;

from area53 import route53

# Instance user names to start, one for each sandbox
users = [
		# 'bart',
		# 'darian',
		# 'david',
		# 'mark',
		# 'derek',
		# 'chris',
		# 'joe',
		# 'barclay',
		# 'karel',
		# 'lucas',
		# 'shire',
    # 'mike'
    'dev1',
    'dev2'
	];



# Map domains to <user>.<domainRoot> ie: shire.dev.groundworkopportunities.org
domain = 'dev.groundworkopportunities.org'


def startInstance():
	global users, ami, key_name, instance_type, security_groups, region_id, aws_access_key_id, aws_secret_access_key, tool_dir, remote_tool_dir, domain;

	conn = boto.ec2.connect_to_region(region_id, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

	reservation = conn.run_instances(ami, key_name=key_name, instance_type=instance_type, security_groups=security_groups, min_count=len(users), max_count=len(users))
	# for i in range(len(users)):
		# conn.create_tags([reservation.instances[i].id], {'Name':users[i]})

	# wait for instances to have running status 
	hosts = open(tool_dir+'/hosts', 'w')
	hosts_aws = open(tool_dir+'/hosts.aws', 'w')
	running_ids = [];
	while(len(running_ids) < len(users)):
		sleep(1)
		for user, instance in zip(users, reservation.instances):
			instance.update()
			if instance.id not in running_ids and instance.state_code == 16: # 16 = running
				# Setup DNS entries for each user
				url = user+'.'+domain+'.'
				zone = route53.get_zone(domain)
				for record in zone.get_records():
					if record.name == url:
						zone.delete_a(url)
				zone.add_a(url, instance.ip_address)
				hosts.write(user+'.'+domain+'\n')
				hosts_aws.write(instance.public_dns_name+'\n')
				print "instance ", instance.id, " name ", user
				conn.create_tags([instance.id], {'Name':user})
				running_ids.append(instance.id)
	hosts.close()
	hosts_aws.close()

	sleep(30)

	for user, instance in zip(users, reservation.instances):
		subprocess.call('pdsh -l ubuntu -R ssh -w '+instance.public_dns_name+' "sudo hostname '+user+'.'+domain+'"', shell=True)

	print 'pdcp -r -l ubuntu -R ssh -w ^'+tool_dir+'/hosts.aws '+tool_dir+'/remote/* '+remote_tool_dir+'/'
	subprocess.call('pdcp -r -l ubuntu -R ssh -w ^'+tool_dir+'/hosts.aws '+tool_dir+'/remote/* '+remote_tool_dir+'/', shell=True)
	print 'pdsh -l ubuntu -R ssh -w ^'+tool_dir+'/hosts.aws "chmod u+x '+remote_tool_dir+'/*.sh; '+remote_tool_dir+'/go-init-sandbox.sh"'
	subprocess.call('pdsh -l ubuntu -R ssh -w ^'+tool_dir+'/hosts.aws "chmod u+x '+remote_tool_dir+'/*.sh; '+remote_tool_dir+'/go-init-sandbox.sh"', shell=True)


def main(argv):
	global users;

	# Commandline arguments...
	opts, args = getopt.getopt(argv,"hu:",["help", "user"])
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			print "\n";
			print "-h|--help 			Display usage\n";
			print "-u|--user <user>		Setup instance for <user>";
			print "\n";
			return 0;
		elif opt in ("-u", "--user"):
			users = [arg];

	startInstance();
	return 0;

if __name__ == "__main__":
   main(sys.argv[1:])
